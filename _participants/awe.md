---
fullname: Olaitan Awe
goby: Olaitan
img: olaitanawe.jpg
links:
  -
    title: LinkedIn
    url: https://www.linkedin.com/in/olaitanawe

affiliation:
  -
    org: University of Ibadan
    position: PhD Candidate
---


# Required:

Fill in your full name for *fullname*, and what you wish to be called during the workshop in *goby*.

Add an image file to the `participants` folder (not `_participants`, like where this file is), which is mostly your face.  Any compressed format is fine (e.g., png, jpg).  See the faculty folder for examples of the appropriate framing and resolution.  Name this file in a way that its obviously connected to you, and put that file name for the *img* key.

# Optional:

If you want to provide any links (e.g., to your homepage, to your publication record), use the *links* element.  This element should be a list, which each item having a *title* (the text that will be displayed) and *url* (the target link).

If you have affiliations you want to include (e.g., employer, school or department), you can fill them as a list with each list element having an *org* and *position* (though *position* is an optional field as well).
